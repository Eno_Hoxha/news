<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<script type="text/javascript" src="js/respond.js"></script>
	<!-- css styles -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="bg">
<!--  body content -->
	<header class="secondary-dark-blue-bg big-zindex">
			 <div class="row">
			 	<div class="col-md-12 menu">
			 		<div>
					 	<img src="img/logo.png" alt="logo"/>
					</div>
					<div>
					<nav>
						<ul>
							<li>
								<a href="#">Home</a>
							</li>
							<li>
								<a href="#">Category</a>
							</li>
							<li>
								<a href="#">Login</a>
							</li>
							<li>
								<a href="#">Contact us</a>
							</li>
						</ul>
					</nav>
					</div>
			</div>
			</div>
	</header>
	<div class="container">
	<div class="row">
		<div class="col-sm-7">
			<div class="all-news">
				<!-- News Block -->
				<div class="news-container row">
					<div class="col-sm-3">
						<div class="image">
							<a href="#">
								<img src="img/oscar.png" alt="oscar" class="img-responsive center-block news-icon" /> 
							</a>
						</div>
					</div>
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-12">
								<div class="news-title">
									<h4>
										<a href="#" >Cold or flu? Here's how to tell, plus..</a>
									</h4>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="news-desc">
									<p>New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
									New Research Sparks Health Canada..
									</p>
								</div>	
							</div>
						</div>
					</div>
				</div>
				<!-- End news block -->
				<!-- News Block -->
				<div class="news-container row">
					<div class="col-sm-3">
						<div class="image">
							<a href="#">
								<img src="img/oscar.png" alt="oscar" class="img-responsive center-block news-icon" /> 
							</a>
						</div>
					</div>
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-12">
								<div class="news-title">
									<h4>
										<a href="#" >Cold or flu? Here's how to tell, plus your flu vaccine questions answered</a>
									</h4>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="news-desc">
									<p>New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
									New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
									New Research Sparks Health Canada Warning Deer Plague Might Infect Humans</p>
								</div>	
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								Date: 2/3/2018
							</div>
							<div class="col-sm-6">
								Country: Albania
							</div>
						</div>
					</div>
				</div>
				<!-- End news block -->
				<!-- News Block -->
				<div class="news-container row">
					<div class="col-sm-3">
						<div class="image">
							<a href="#">
								<img src="img/oscar.png" alt="oscar" class="img-responsive center-block news-icon" /> 
							</a>
						</div>
					</div>
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-12">
								<div class="news-title">
									<h4>
										<a href="#" >Cold or flu? Here's how to tell, plus your flu vaccine questions answered</a>
									</h4>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="news-desc">
									<p>New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
									New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
									New Research Sparks Health Canada Warning Deer Plague Might Infect Humans</p>
								</div>	
							</div>
						</div>
					</div>
				</div>
				<!-- End news block -->
			</div>
		</div>
		<div class="col-sm-4 col-sm-offset-1">
			<div class="recomandations all-news">
				<div class="news-container">
					<div class="row">
						<div class="col-sm-12">
							<div class="image">
								<a href="#">
									<img src="img/oscar.png" alt="oscar" class="img-responsive center-block news-icon" /> 
								</a>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-sm-12">
							<div class="row">
								<div class="col-lg-12">
									<div class="news-title">
										<h4>
											<a href="#" >Cold or flu? Here's how to tell, plus your flu vaccine questions answered</a>
										</h4>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="news-desc">
										<p>New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
										New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
										New Research Sparks Health Canada Warning Deer Plague Might Infect Humans</p>
									</div>	
								</div>
							</div>
					</div>
					</div>
				</div>
				<div class="news-container">
					<div class="row">
						<div class="col-sm-12">
							<div class="image">
								<a href="#">
									<img src="img/oscar.png" alt="oscar" class="img-responsive center-block news-icon" /> 
								</a>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-sm-12">
							<div class="row">
								<div class="col-lg-12">
									<div class="news-title">
										<h4>
											<a href="#" >Cold or flu? Here's how to tell, plus your flu vaccine questions answered</a>
										</h4>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="news-desc">
										<p>New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
										New Research Sparks Health Canada Warning Deer Plague Might Infect Humans
										New Research Sparks Health Canada Warning Deer Plague Might Infect Humans</p>
									</div>	
								</div>
							</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
<!--  javascripts -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>

</body>
</html>